package com.mutsuddi_s.bs_assesment.data.mapper

import com.mutsuddi_s.bs_assesment.common.Constants.formattedDateTime
import com.mutsuddi_s.bs_assesment.data.dto.ItemDto
import com.mutsuddi_s.bs_assesment.data.dto.OwnerDto
import com.mutsuddi_s.bs_assesment.domain.model.Item
import com.mutsuddi_s.bs_assesment.domain.model.Owner

fun ItemDto.toItem(): Item {
    val owner = this.owner.toOwner()
    return Item(
        id = id,
        name = name,
        description = description,
        owner = owner,
        updatedAt =formattedDateTime(updatedAt)
    )
}


fun OwnerDto.toOwner(): Owner {
    return Owner(
        id = id,
        login = login,
        avatarUrl = avatarUrl
    )
}

