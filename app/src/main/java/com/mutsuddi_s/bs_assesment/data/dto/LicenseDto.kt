package com.mutsuddi_s.bs_assesment.data.dto

data class LicenseDto(
    val key: String,
    val name: String,
    val node_id: String,
    val spdx_id: String,
    val url: String
)