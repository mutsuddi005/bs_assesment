package com.mutsuddi_s.bs_assesment.data.network

import com.mutsuddi_s.bs_assesment.data.dto.RepositoriesDto
import retrofit2.http.GET
import retrofit2.http.Query

interface GithubApi {
    @GET("search/repositories")
    suspend fun getRepositories(
        @Query("q") query: String?,
        @Query("sort") sort: String?
    ):RepositoriesDto
}