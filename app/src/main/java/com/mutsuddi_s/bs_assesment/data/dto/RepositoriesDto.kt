package com.mutsuddi_s.bs_assesment.data.dto

data class RepositoriesDto(
    val incomplete_results: Boolean,
    val items: List<ItemDto>,
    val total_count: Int
)