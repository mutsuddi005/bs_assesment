package com.mutsuddi_s.bs_assesment.data.repository

import com.mutsuddi_s.bs_assesment.data.dto.RepositoriesDto
import com.mutsuddi_s.bs_assesment.data.network.GithubApi
import com.mutsuddi_s.bs_assesment.domain.repository.GithubRepository
import javax.inject.Inject

class GithubRepositoryImpl @Inject constructor(private val githubApi: GithubApi): GithubRepository {
    override suspend fun getRepositories(query:String,sort: String): RepositoriesDto {
        return githubApi.getRepositories(query,sort)
    }
}