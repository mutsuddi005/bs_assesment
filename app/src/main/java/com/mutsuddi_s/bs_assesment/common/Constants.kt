package com.mutsuddi_s.bs_assesment.common

import java.text.SimpleDateFormat
import java.util.Locale

object Constants {
    const val BASE_URL = "https://api.github.com/"

    const val QUERY_PARAM_q="Android"
    const val QUERY_PARAM_sort="stars"

    private val inputFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.getDefault())
    private val outputFormat = SimpleDateFormat("MM-dd-yy HH:mm", Locale.getDefault())

    fun formattedDateTime(updatedAt: String): String {
        return try {
            val parsedDate = inputFormat.parse(updatedAt)
            parsedDate?.let {
                outputFormat.format(parsedDate)
            } ?: ""
        } catch (e: Exception) {
            updatedAt // Return original string if parsing fails
        }
    }
}