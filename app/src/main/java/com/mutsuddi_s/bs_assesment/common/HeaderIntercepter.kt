package com.mutsuddi_s.bs_assesment.common

import android.content.Context
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

class HeaderInterceptor @Inject constructor(private val context: Context) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()
        val newRequest = originalRequest.newBuilder()
            .header("Header", context.packageName)
            .build()
        return chain.proceed(newRequest)
    }
}