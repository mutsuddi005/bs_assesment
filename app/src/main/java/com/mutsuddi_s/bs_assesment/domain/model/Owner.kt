package com.mutsuddi_s.bs_assesment.domain.model

data class Owner(
    val id: Int,
    val login: String,
    val avatarUrl: String
)
