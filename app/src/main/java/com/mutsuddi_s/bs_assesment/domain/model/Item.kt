package com.mutsuddi_s.bs_assesment.domain.model

data class Item(
    val id: Int,
    val name: String,
    val description: String,
    val owner: Owner,
    val updatedAt: String,
)