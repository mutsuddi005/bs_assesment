package com.mutsuddi_s.bs_assesment.domain.usecase

import com.mutsuddi_s.bs_assesment.common.Resource
import com.mutsuddi_s.bs_assesment.data.mapper.toItem
import com.mutsuddi_s.bs_assesment.domain.model.Item
import com.mutsuddi_s.bs_assesment.domain.repository.GithubRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

class GetRepositoriesUseCase @Inject constructor(private val repository: GithubRepository) {
    operator fun invoke(query:String,sort:String): Flow<Resource<List<Item>>> = flow{

        try {
            emit(Resource.Loading<List<Item>>())
            val repos = repository.getRepositories(query,sort).items.map { it.toItem() }
            emit(Resource.Success<List<Item>>(repos))
        } catch(e: HttpException) {
            emit(Resource.Error<List<Item>>(e.localizedMessage ?: "An unexpected error occured"))
        } catch(e: IOException) {
            emit(Resource.Error<List<Item>>("Couldn't reach server. Check your internet connection."))
        }
    }
}