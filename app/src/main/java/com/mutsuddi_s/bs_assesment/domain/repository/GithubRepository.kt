package com.mutsuddi_s.bs_assesment.domain.repository

import com.mutsuddi_s.bs_assesment.data.dto.RepositoriesDto

interface GithubRepository {
    suspend fun getRepositories(query:String,sort: String):RepositoriesDto
}