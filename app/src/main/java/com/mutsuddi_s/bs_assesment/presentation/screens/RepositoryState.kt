package com.mutsuddi_s.bs_assesment.presentation.screens

import com.mutsuddi_s.bs_assesment.domain.model.Item

data class RepositoryState(
    val isLoading: Boolean= false,
    val repositories: List<Item> = emptyList(),
    val error: String?=""
)