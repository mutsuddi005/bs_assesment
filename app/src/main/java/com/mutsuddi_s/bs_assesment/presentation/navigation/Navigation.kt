package com.mutsuddi_s.bs_assesment.presentation.navigation

import androidx.compose.runtime.Composable
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.mutsuddi_s.bs_assesment.presentation.screens.RepositoryViewModel
import com.mutsuddi_s.bs_assesment.presentation.screens.repository_detail.components.RepoDetailsScreen
import com.mutsuddi_s.bs_assesment.presentation.screens.repository_list.components.RepositoryListScreen


@Composable
fun Navigation(){


    val navController = rememberNavController()
    val repositoryViewModel: RepositoryViewModel= hiltViewModel()

    NavHost(navController = navController, startDestination = Screens.RepoListScreen.route ){
        composable(
            route=Screens.RepoListScreen.route
        ){

            RepositoryListScreen(navController,repositoryViewModel)
        }
        composable(
            route=Screens.RepoDetailsScreen.route
        ){

            RepoDetailsScreen(viewModel = repositoryViewModel)

        }


    }


}