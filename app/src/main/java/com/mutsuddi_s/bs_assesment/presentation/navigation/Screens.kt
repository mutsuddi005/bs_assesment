package com.mutsuddi_s.bs_assesment.presentation.navigation

sealed class Screens (val route: String){
    object RepoListScreen : Screens("repository_list_screen")
    object RepoDetailsScreen : Screens("repository_detail_screen")
}