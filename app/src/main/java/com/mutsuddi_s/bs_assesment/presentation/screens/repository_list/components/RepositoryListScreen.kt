package com.mutsuddi_s.bs_assesment.presentation.screens.repository_list.components

import android.annotation.SuppressLint
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Divider
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.navigation.NavController
import com.mutsuddi_s.bs_assesment.presentation.navigation.Screens
import com.mutsuddi_s.bs_assesment.presentation.screens.RepositoryViewModel

@OptIn(ExperimentalMaterial3Api::class)
@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@Composable
fun RepositoryListScreen(
    navController: NavController,
    viewModel: RepositoryViewModel
) {

    val state = viewModel.state.value

    Scaffold(topBar = {
        TopAppBar(
            colors = TopAppBarDefaults.smallTopAppBarColors(
                containerColor = MaterialTheme.colorScheme.primaryContainer,
                titleContentColor = MaterialTheme.colorScheme.primary,
            ),
            title = {
                Text("Repository List", style = MaterialTheme.typography.bodyLarge)
            }
        )
    }) { innerPadding ->
        if (state.isLoading) {
            Box(modifier = Modifier
                .padding(innerPadding)
                .fillMaxSize()) {
                CircularProgressIndicator(Modifier.align(Alignment.Center))
            }
        } else if (!state.error.isNullOrEmpty()) {
            Box(modifier = Modifier
                .fillMaxSize()
                .padding(innerPadding)) {
                Text(state.error.toString(), modifier = Modifier.align(Alignment.Center))
            }
        }

        state.repositories?.let {
            Box(modifier = Modifier
                .fillMaxSize()
                .padding(innerPadding)) {
                LazyColumn(modifier = Modifier.fillMaxSize()) {
                    items(state.repositories) { repoItem ->
                        RepositoryListItem(repository = repoItem, onItemClick = {
                            viewModel.setSelectedRepository(repoItem)
                            navController.navigate(Screens.RepoDetailsScreen.route)
                        })

                        Divider()


                    }

                }
            }

        }

    }
}
