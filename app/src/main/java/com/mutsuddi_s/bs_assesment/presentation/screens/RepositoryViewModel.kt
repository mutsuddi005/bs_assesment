package com.mutsuddi_s.bs_assesment.presentation.screens

import android.util.Log
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mutsuddi_s.bs_assesment.common.Constants.QUERY_PARAM_q
import com.mutsuddi_s.bs_assesment.common.Constants.QUERY_PARAM_sort
import com.mutsuddi_s.bs_assesment.common.Resource
import com.mutsuddi_s.bs_assesment.domain.model.Item
import com.mutsuddi_s.bs_assesment.domain.usecase.GetRepositoriesUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class RepositoryViewModel @Inject constructor(
    private val useCase: GetRepositoriesUseCase
): ViewModel() {

    private val _state = mutableStateOf(RepositoryState())
    val state: State<RepositoryState> = _state

    private val _selectedRepository = mutableStateOf<Item?>(null)
    val selectedRepository: State<Item?> = _selectedRepository

    init {

        getAllRepositories(QUERY_PARAM_q,QUERY_PARAM_sort)
    }

    private fun getAllRepositories(query:String,sort:String){
        useCase(query,sort).onEach {  result ->
            when (result) {
                is Resource.Success -> {
                    _state.value = RepositoryState(repositories = result.data ?: emptyList())
                }
                is Resource.Error -> {
                    _state.value = RepositoryState(
                        error = result.message ?: "An unexpected error occured"
                    )
                }
                is Resource.Loading -> {
                    _state.value = RepositoryState(isLoading = true)
                }
            }
        }.launchIn(viewModelScope)


    }

    fun setSelectedRepository(item: Item?) {
        _selectedRepository.value = item
        Log.d("viewmodel", "${item?.name} ")
    }
}