package com.mutsuddi_s.bs_assesment.di

import android.app.Application
import com.mutsuddi_s.bs_assesment.common.Constants.BASE_URL
import com.mutsuddi_s.bs_assesment.common.HeaderInterceptor
import com.mutsuddi_s.bs_assesment.data.network.GithubApi
import com.mutsuddi_s.bs_assesment.data.repository.GithubRepositoryImpl
import com.mutsuddi_s.bs_assesment.domain.repository.GithubRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class AppModule  {

    @Provides
    @Singleton
    fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {
        return HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }
    }

    @Provides
    @Singleton
    fun provideOkHttpClient(
        loggingInterceptor: HttpLoggingInterceptor,
        headerInterceptor: HeaderInterceptor
                           ): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .addInterceptor(headerInterceptor)
            .build()
    }


    @Provides
    @Singleton
    fun provideRetrofitInstance(okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BASE_URL)
            .client(okHttpClient) // Set the OkHttpClient with the interceptor
            .build()
    }

    @Provides
    @Singleton
    fun provideHeaderInterceptor(application: Application): HeaderInterceptor {
        return HeaderInterceptor(application.applicationContext)
    }




    @Provides
    @Singleton
    fun provideGithubApi(retrofit: Retrofit) : GithubApi =
        retrofit.create(GithubApi::class.java)
    @Provides
    @Singleton
    fun provideGithubRepository(githubApi: GithubApi) : GithubRepository {
        return GithubRepositoryImpl(githubApi)
    }
}